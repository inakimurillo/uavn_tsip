#ifndef H_COMMON_TYPES_INCLUDED
#define H_COMMON_TYPES_INCLUDED

#include <stdint.h>

#define DLE_BYTE 0x10
#define ETX_BYTE 0x03

typedef struct {
  uint32_t length;
  uint8_t *buffer;
}tsip_packet;

typedef enum parser_err{
  PARSE_OK,
  INTEGRITY_ERR,
  DLE_EXTRACTION_ERR,
  CRC_ERR
}parser_err;

#endif
