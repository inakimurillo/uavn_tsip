#include <stdio.h>
#include <stdint.h>
#include "uav_functions.h"
#include "tests/tests.h"

#define MAX_ITERATION 10

int main(void)
{
  uint32_t iteration = 0;
  uint32_t error_counter = 0;

  printf(" --- Simutalion START ---\n\n");

  /// Sistem simulation
  while(iteration < MAX_ITERATION)
  {
    TaskUplink200Hz();
    iteration++;
  }
  printf("\n --- Simutalion END ---\n\n\n");

  printf(" --- TEST START ---\n\n");
  perform_test(TEST_1_delete_byte, "TEST_1_delete_byte", &error_counter);
  perform_test(TEST_2_delete_byte, "TEST_2_delete_byte", &error_counter);
  perform_test(TEST_3_delete_byte, "TEST_3_delete_byte", &error_counter);
  perform_test(TEST_4_delete_byte, "TEST_4_delete_byte", &error_counter);

  printf("\n");

  perform_test(TEST_1_find_pattern, "TEST_1_find_pattern", &error_counter);
  perform_test(TEST_2_find_pattern, "TEST_2_find_pattern", &error_counter);

  printf("\n");

  perform_test(TEST_1_crc_check, "TEST_1_crc_check", &error_counter);
  perform_test(TEST_2_crc_check, "TEST_2_crc_check", &error_counter);

  printf("\n");

  perform_test(TEST_1_dle_etx_extraction, "TEST_1_dle_etx_extraction", &error_counter);
  perform_test(TEST_2_dle_etx_extraction, "TEST_2_dle_etx_extraction", &error_counter);
  perform_test(TEST_3_dle_etx_extraction, "TEST_3_dle_etx_extraction", &error_counter);

  printf("\n");

  perform_test(TEST_1_integrity_check, "TEST_1_integrity_check", &error_counter);
  perform_test(TEST_2_integrity_check, "TEST_2_integrity_check", &error_counter);
  perform_test(TEST_3_integrity_check, "TEST_3_integrity_check", &error_counter);
  perform_test(TEST_4_integrity_check, "TEST_4_integrity_check", &error_counter);
  perform_test(TEST_5_integrity_check, "TEST_5_integrity_check", &error_counter);
  perform_test(TEST_6_integrity_check, "TEST_6_integrity_check", &error_counter);

  printf("\n");

  perform_test(TEST_1_tsip_packet_parser, "TEST_1_tsip_packet_parser", &error_counter);
  perform_test(TEST_2_tsip_packet_parser, "TEST_2_tsip_packet_parser", &error_counter);
  perform_test(TEST_3_tsip_packet_parser, "TEST_3_tsip_packet_parser", &error_counter);
  perform_test(TEST_4_tsip_packet_parser, "TEST_4_tsip_packet_parser", &error_counter);
  perform_test(TEST_5_tsip_packet_parser, "TEST_5_tsip_packet_parser", &error_counter);
  perform_test(TEST_6_tsip_packet_parser, "TEST_6_tsip_packet_parser", &error_counter);

  printf("\n\nTotal ERROR: %d\n", error_counter);


  return 0;
}
