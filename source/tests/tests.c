#include <stdio.h>
#include "../tsip_parser.h"
#include "../tools.h"

void perform_test(uint32_t (*func_ptr)(void), char *test_name,uint32_t *error_counter)
{
  uint32_t current_err = 0;
  current_err = func_ptr();
  if(current_err > 0)
  {
    printf("[FAIL] %s\n", test_name);
  }
  else
  {
    printf("[PASS] %s\n", test_name);
  }
  *error_counter = (*error_counter + current_err);
}

/// delete first byte
uint32_t TEST_1_delete_byte(void)
{
  uint32_t index;
  uint8_t in_data[8] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08};
  uint8_t expected_result[8] = {0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x00};

  delete_byte(in_data, 8, 0);

  for (index = 0; index < 8; index++)
  {
    if(in_data[index] != expected_result[index])
    {
      return 1;
    }
  }

  return 0;
}

/// Delete last byte
uint32_t TEST_2_delete_byte(void)
{
  uint32_t index;
  uint8_t in_data[8] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08};
  uint8_t expected_result[8] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x00};

  delete_byte(in_data, 8, 7);

  for (index = 0; index < 8; index++)
  {
    if(in_data[index] != expected_result[index])
    {
      return 1;
    }
  }

  return 0;
}

/// delete arbitrary byte
uint32_t TEST_3_delete_byte(void)
{
  uint32_t index;
  uint8_t in_data[8] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08};
  uint8_t expected_result[8] =  {0x01, 0x02, 0x03, 0x05, 0x06, 0x07, 0x08, 0x00};

  delete_byte(in_data, 8, 3);

  for (index = 0; index < 8; index++)
  {
    if(in_data[index] != expected_result[index])
    {
      return 1;
    }
  }

  return 0;
}

/// Do not deletel with pos > len
uint32_t TEST_4_delete_byte(void)
{
  uint32_t index;
  uint8_t in_data[8] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08};
  uint8_t expected_result[8] =  {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08};

  delete_byte(in_data, 8, 9);

  for (index = 0; index < 8; index++)
  {
    if(in_data[index] != expected_result[index])
    {
      return 1;
    }
  }

  return 0;
}

/// find existing pattern
uint32_t TEST_1_find_pattern(void)
{
  uint8_t in_data[8] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08};
  uint32_t result;
  uint32_t expected_result = 3;
  uint32_t returned;
  uint32_t returned_expected = 1;

  returned = find_pattern(in_data, 8, 0x04, 0x05, &result);

  if(returned != returned_expected)
  {
    return 1;
  }

  if(result != expected_result)
  {
    return 1;
  }

  return 0;
}

/// do not find not existing pattern
uint32_t TEST_2_find_pattern(void)
{
  uint8_t in_data[8] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08};
  uint32_t result;
  uint32_t returned;
  uint32_t returned_expected = 0;

  returned = find_pattern(in_data, 8, 0x01, 0x04, &result);

  if(returned != returned_expected)
  {
    return 1;
  }

  return 0;
}

/// CRC OK
uint32_t TEST_1_crc_check(void)
{
  uint8_t in_data[14] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x7B, 0x57, 0x20, 0x25};
  uint32_t returned;
  uint32_t returned_expected = 0;

  returned = crc_check(in_data,14);

  if(returned != returned_expected)
  {
    return 1;
  }

  return 0;
}

/// CRC NOK
uint32_t TEST_2_crc_check(void)
{
  uint8_t in_data[14] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x7B, 0x57, 0xFF, 0xFF};
  uint32_t returned;
  uint32_t returned_expected = 1;

  returned = crc_check(in_data,14);

  if(returned != returned_expected)
  {
    return 1;
  }

  return 0;
}

/// try to extract from an invalid packet -> error is ok
uint32_t TEST_1_dle_etx_extraction(void)
{
  uint32_t index;
  uint8_t in_data[8] = {0x01, 0x02, 0x03, 0x10, 0x10, 0x06, 0x07, 0x08};
  uint8_t expected_result[8] = {0x01, 0x02, 0x03, 0x10, 0x10, 0x06, 0x07, 0x08};
  uint32_t returned;
  uint32_t returned_expected = 1;
  uint32_t len = 8;
  uint32_t expected_len = 8;

  returned = dle_etx_extraction(in_data, &len);
  if(returned != returned_expected)
  {
    return 1;
  }

  for (index = 0; index < 8; index++)
  {
    if(in_data[index] != expected_result[index])
    {
      return 1;
    }
  }

  if(len != expected_len)
  {
    return 1;
  }

  return 0;
}

/// correct one extraction
uint32_t TEST_2_dle_etx_extraction(void)
{
  uint32_t index;
  uint8_t in_data[17] = {0x10,0x04,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0x03};
  uint8_t expected_result[17] = {0x04,0x03,0x02,0x01,0x10,0xA4,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x00, 0x00, 0x00, 0x00,0x00};
  uint32_t returned;
  uint32_t returned_expected = 0;
  uint32_t len = 17;
  uint32_t expected_len = 12;

  returned = dle_etx_extraction(in_data, &len);
  if(returned != returned_expected)
  {
    return 1;
  }

  for (index = 0; index < 8; index++)
  {
    if(in_data[index] != expected_result[index])
    {
      return 1;
    }
  }

  if(len != expected_len)
  {
    return 1;
  }

  return 0;
}

/// DLE DLE ETX inside
uint32_t TEST_3_dle_etx_extraction(void)
{
  uint32_t index;
  uint8_t in_data[17] = {0x10,0x04,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0x03,0x29,0x21,0x8F,0xB3,0x10,0x03};
  uint8_t expected_result[17] = {0x04,0x03,0x02,0x01,0x10,0xA4,0x10,0x03,0x29,0x21,0x8F,0xB3,0x00, 0x00, 0x00, 0x00,0x00};
  uint32_t returned;
  uint32_t returned_expected = 0;
  uint32_t len = 17;
  uint32_t expected_len = 12;

  returned = dle_etx_extraction(in_data, &len);
  if(returned != returned_expected)
  {
    return 1;
  }

  for (index = 0; index < 8; index++)
  {
    if(in_data[index] != expected_result[index])
    {
      return 1;
    }
  }

  if(len != expected_len)
  {
    return 1;
  }

  return 0;
}

/// check for integrity in ok frame
uint32_t TEST_1_integrity_check(void)
{
  uint32_t index;
  uint8_t in_data[17] = {0x10,0x04,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0x03};
  uint8_t expected_data[17] = {0x10,0x04,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0x03};
  uint32_t returned;
  uint32_t returned_expected = 0;
  uint32_t len = 17;

  returned = integrity_check(in_data, len);

  if(returned != returned_expected)
  {
    return 1;
  }

  for (index = 0; index < len; index++)
  {
    if(in_data[index] != expected_data[index])
    {
      return 1;
    }
  }

  return 0;
}

/// Not starting with DLE
uint32_t TEST_2_integrity_check(void)
{
  uint32_t index;
  uint8_t in_data[17] = {0xAA,0x04,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0x03};
  uint8_t expected_data[17] = {0xAA,0x04,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0x03};
  uint32_t returned;
  uint32_t returned_expected = 1;
  uint32_t len = 17;

  returned = integrity_check(in_data, len);

  if(returned != returned_expected)
  {
    return 1;
  }

  for (index = 0; index < len; index++)
  {
    if(in_data[index] != expected_data[index])
    {
      return 1;
    }
  }

  return 0;
}

/// ID1 byte must be different from code DLE
uint32_t TEST_3_integrity_check(void)
{
  uint32_t index;
  uint8_t in_data[17] = {0x10,0x10,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0x03};
  uint8_t expected_data[17] = {0x10,0x10,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0x03};
  uint32_t returned;
  uint32_t returned_expected = 1;
  uint32_t len = 17;

  returned = integrity_check(in_data, len);

  if(returned != returned_expected)
  {
    return 1;
  }

  for (index = 0; index < len; index++)
  {
    if(in_data[index] != expected_data[index])
    {
      return 1;
    }
  }

  return 0;
}

/// ID1 byte must be different from code ETX
uint32_t TEST_4_integrity_check(void)
{
  uint32_t index;
  uint8_t in_data[17] = {0x10,0x03,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0x03};
  uint8_t expected_data[17] = {0x10,0x03,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0x03};
  uint32_t returned;
  uint32_t returned_expected = 1;
  uint32_t len = 17;

  returned = integrity_check(in_data, len);

  if(returned != returned_expected)
  {
    return 1;
  }

  for (index = 0; index < len; index++)
  {
    if(in_data[index] != expected_data[index])
    {
      return 1;
    }
  }

  return 0;
}

// Packets end on DLE, ETX combination
uint32_t TEST_5_integrity_check(void)
{
  uint32_t index;
  uint8_t in_data[17] = {0x10,0x04,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0xAA};
  uint8_t expected_data[17] = {0x10,0x04,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0xAA};
  uint32_t returned;
  uint32_t returned_expected = 1;
  uint32_t len = 17;

  returned = integrity_check(in_data, len);

  if(returned != returned_expected)
  {
    return 1;
  }

  for (index = 0; index < len; index++)
  {
    if(in_data[index] != expected_data[index])
    {
      return 1;
    }
  }

  return 0;
}

/// DLE correctly excaped
uint32_t TEST_6_integrity_check(void)
{
  uint32_t index;
  uint8_t in_data[17] = {0x10,0x04,0x03,0x02,0x01,0x55,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0xAA};
  uint8_t expected_data[17] = {0x10,0x04,0x03,0x02,0x01,0x55,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0xAA};
  uint32_t returned;
  uint32_t returned_expected = 1;
  uint32_t len = 17;

  returned = integrity_check(in_data, len);

  if(returned != returned_expected)
  {
    return 1;
  }

  for (index = 0; index < len; index++)
  {
    if(in_data[index] != expected_data[index])
    {
      return 1;
    }
  }

  return 0;
}


/// check for integrity in ok frame
uint32_t TEST_1_tsip_packet_parser(void)
{
  uint32_t index;
  uint8_t in_data[17] = {0x10,0x04,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0x03};
  uint8_t expected_data[17] = {0x02,0x01,0x10,0xA4,0x10,0xE1};
  parser_err returned;
  parser_err returned_expected = PARSE_OK;
  uint32_t len = 17;
  tsip_packet packet_p;

  packet_p.buffer = in_data;
  packet_p.length = len;

  returned = tsip_packet_parser(&packet_p);

  if(returned != returned_expected)
  {
    return 1;
  }

  for (index = 0; index < len; index++)
  {
    if(in_data[index] != expected_data[index])
    {
      return 1;
    }
  }

  return 0;
}

/// Not starting with DLE
uint32_t TEST_2_tsip_packet_parser(void)
{
  uint32_t index;
  uint8_t in_data[17] = {0xAA,0x04,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0x03};
  uint8_t expected_data[17] = {0xAA,0x04,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0x03};
  parser_err returned;
  parser_err returned_expected = INTEGRITY_ERR;
  uint32_t len = 17;
  tsip_packet packet_p;

  packet_p.buffer = in_data;
  packet_p.length = len;

  returned = tsip_packet_parser(&packet_p);

  if(returned != returned_expected)
  {
    return 1;
  }

  for (index = 0; index < len; index++)
  {
    if(in_data[index] != expected_data[index])
    {
      return 1;
    }
  }

  return 0;
}

/// ID1 byte must be different from code DLE
uint32_t TEST_3_tsip_packet_parser(void)
{
  uint32_t index;
  uint8_t in_data[17] = {0x10,0x10,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0x03};
  uint8_t expected_data[17] = {0x10,0x10,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0x03};
  parser_err returned;
  parser_err returned_expected = INTEGRITY_ERR;
  uint32_t len = 17;
  tsip_packet packet_p;

  packet_p.buffer = in_data;
  packet_p.length = len;

  returned = tsip_packet_parser(&packet_p);

  if(returned != returned_expected)
  {
    return 1;
  }

  for (index = 0; index < len; index++)
  {
    if(in_data[index] != expected_data[index])
    {
      return 1;
    }
  }

  return 0;
}

/// ID1 byte must be different from code ETX
uint32_t TEST_4_tsip_packet_parser(void)
{
  uint32_t index;
  uint8_t in_data[17] = {0x10,0x03,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0x03};
  uint8_t expected_data[17] = {0x10,0x03,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0x03};
  parser_err returned;
  parser_err returned_expected = INTEGRITY_ERR;
  uint32_t len = 17;
  tsip_packet packet_p;

  packet_p.buffer = in_data;
  packet_p.length = len;

  returned = tsip_packet_parser(&packet_p);

  if(returned != returned_expected)
  {
    return 1;
  }

  for (index = 0; index < len; index++)
  {
    if(in_data[index] != expected_data[index])
    {
      return 1;
    }
  }

  return 0;
}

// Packets end on DLE, ETX combination
uint32_t TEST_5_tsip_packet_parser(void)
{
  uint32_t index;
  uint8_t in_data[17] = {0x10,0x04,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0xAA};
  uint8_t expected_data[17] = {0x10,0x04,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0xAA};
  parser_err returned;
  parser_err returned_expected = INTEGRITY_ERR;
  uint32_t len = 17;
  tsip_packet packet_p;

  packet_p.buffer = in_data;
  packet_p.length = len;

  returned = tsip_packet_parser(&packet_p);

  if(returned != returned_expected)
  {
    return 1;
  }

  for (index = 0; index < len; index++)
  {
    if(in_data[index] != expected_data[index])
    {
      return 1;
    }
  }

  return 0;
}

/// Bad CRC
uint32_t TEST_6_tsip_packet_parser(void)
{
  uint32_t index;
  uint8_t in_data[17] = {0x10,0x04,0x03,0x02,0x01,0x10,0x10,0xFF,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0x03};
  uint8_t expected_data[17] = {0x04,0x03,0x02,0x01,0x10,0xFF,0x10,0xE1,0x29,0x21,0x8F,0xB3};
  parser_err returned;
  parser_err returned_expected = CRC_ERR;
  uint32_t len = 17;
  tsip_packet packet_p;

  packet_p.buffer = in_data;
  packet_p.length = len;

  returned = tsip_packet_parser(&packet_p);
  if(returned != returned_expected)
  {
    return 1;
  }

  for (index = 0; index < len; index++)
  {
    if(in_data[index] != expected_data[index])
    {
      return 1;
    }
  }

  return 0;
}
