#include "../common_types.h"

void perform_test(uint32_t (*func_ptr)(void), char *test_name,uint32_t *error_counter);

uint32_t TEST_1_delete_byte(void);
uint32_t TEST_2_delete_byte(void);
uint32_t TEST_3_delete_byte(void);
uint32_t TEST_4_delete_byte(void);

uint32_t TEST_1_find_pattern(void);
uint32_t TEST_2_find_pattern(void);

uint32_t TEST_1_crc_check(void);
uint32_t TEST_2_crc_check(void);

uint32_t TEST_1_dle_etx_extraction(void);
uint32_t TEST_2_dle_etx_extraction(void);
uint32_t TEST_3_dle_etx_extraction(void);

uint32_t TEST_1_integrity_check(void);
uint32_t TEST_2_integrity_check(void);
uint32_t TEST_3_integrity_check(void);
uint32_t TEST_4_integrity_check(void);
uint32_t TEST_5_integrity_check(void);
uint32_t TEST_6_integrity_check(void);

uint32_t TEST_1_tsip_packet_parser(void);
uint32_t TEST_2_tsip_packet_parser(void);
uint32_t TEST_3_tsip_packet_parser(void);
uint32_t TEST_4_tsip_packet_parser(void);
uint32_t TEST_5_tsip_packet_parser(void);
uint32_t TEST_6_tsip_packet_parser(void);
