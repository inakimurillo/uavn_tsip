#ifndef H_TSIP_TOOLS_INCLUDED
#define H_TSIP_TOOLS_INCLUDED
#include "common_types.h"

/**
 * \brief Check the CRC of a given array
 *
 * The given length must be at least 5 bytes (1 data byte plus 4 CRC bytes)
 *
 * \param[in] data Pointer to the array
 * \param[in] length Number of bytes in the data array
 */
uint32_t crc_check(uint8_t *data, uint32_t length);

/**
 * \brief CRC calculation for TSIP packets.
 *
 * \param buf   Pointer to buffer containing data to process.
 * \param start Offset value of first data byte (within buf) to be processed.
 * \param end   Offset value of last data byte (within buf) to be processed.
 * \return      Calculated CRC value.
 */
uint32_t crc32(uint8_t *buf, uint32_t start, uint32_t end);

/**
 * \brief Deletes the selected byte
 *
 * \param[in] data Pointer to the array
 * \param[in] length Number of bytes in the data array
 * \param[in] pos Position to delete
 */
void delete_byte(uint8_t *data, uint32_t len, uint32_t pos);

/**
 * \brief Find the patter inside the array
 *
 * \param[in] data Pointer to the array
 * \param[in] length Number of bytes in the data array
 * \param[in] byte1 First byte to find
 * \param[in] byte2 Second byte to find
 * \param[out] pos Position of the fisrt byte
 * \return 1 if found, 0 otherwise
 */
uint32_t find_pattern(uint8_t *data, uint32_t length, uint8_t byte1, uint8_t byte2, uint32_t *pos);

#endif
