#include "tsip_parser.h"
#include "tools.h"

#define MESSAGE_MIN_LENGTH 9
#define START_DLE_POS 0
#define ID1_POS 1
#define ID2_POS 2



uint32_t dle_etx_extraction (uint8_t *data, uint32_t *length)
{
  uint32_t found;
  uint32_t pos;

  ///Delete first DLE byte
  if(data[0] != DLE_BYTE)
  {
    return 1;
  }
  else
  {
    delete_byte(data, *length, 0);
    *length = (*length - 1);
  }

  /// Find DLE DLE pattern
  do {
    found = find_pattern(data, *length, DLE_BYTE, DLE_BYTE, &pos);
    if(found == 1)
    {
      delete_byte(data, *length, pos);
      *length = (*length - 1);
    }
  } while(found == 1);

  /// delete last byte ETX
  delete_byte(data, *length, *length -1);
  *length = (*length - 1);


  /// delete last byte DLE
  delete_byte(data, *length, *length -1);
  *length = (*length - 1);

  return 0;
}

uint32_t integrity_check (uint8_t *data, uint32_t length)
{
  uint32_t current_pos = 0;


  if(length < MESSAGE_MIN_LENGTH)
  {
    return 1;
  }

  if(data[START_DLE_POS] != DLE_BYTE)
  {
    return 1;
  }

  if((data[ID1_POS] ==  DLE_BYTE) || (data[ID1_POS] ==  ETX_BYTE))
  {
    return 1;
  }

  if(((data[length - 2] != DLE_BYTE)) || ((data[length -1] != ETX_BYTE)))
  {
    return 1;
  }

  // skip last two bytes
  for(current_pos = ID2_POS; current_pos < (length - 2); current_pos++)
  {
    // printf("data[%d] = 0x%02X\n", current_pos,data[current_pos]);
    // printf("data[current_pos+1] = 0x%02X\n", data[current_pos+1]);
    if((data[current_pos] == DLE_BYTE) && (data[current_pos + 1] != DLE_BYTE))
    {
      return 1;
    }
    else if((data[current_pos] == DLE_BYTE) && (data[current_pos + 1] == DLE_BYTE))
    {
      current_pos++;
    }
    else
    {
      /// Do nothing
    }

  }

  return 0;
}

parser_err tsip_packet_parser(tsip_packet *packet_p)
{
  uint32_t error_counter = 0;

  /// Integreity check
  error_counter = integrity_check(packet_p->buffer, packet_p->length);
  if(error_counter > 0)
  {
    return INTEGRITY_ERR;
  }

  // /// ETX extraction
  // delete_byte(packet_p->buffer, packet_p->length, (packet_p->length -1));
  // packet_p->length--;

  error_counter = dle_etx_extraction(packet_p->buffer, &packet_p->length);
  if(error_counter > 0)
  {
    return DLE_EXTRACTION_ERR;
  }

  error_counter = crc_check(packet_p->buffer, packet_p->length);
  if(error_counter > 0)
  {
    return CRC_ERR;
  }

  /// ID1 extraction
  delete_byte(packet_p->buffer, packet_p->length, 0);
  packet_p->length--;

  /// ID2 extraction
  delete_byte(packet_p->buffer, packet_p->length, 0);
  packet_p->length--;

  /// CRC extraction
  delete_byte(packet_p->buffer, packet_p->length, (packet_p->length -1));
  packet_p->length--;
  delete_byte(packet_p->buffer, packet_p->length, (packet_p->length -1));
  packet_p->length--;
  delete_byte(packet_p->buffer, packet_p->length, (packet_p->length -1));
  packet_p->length--;
  delete_byte(packet_p->buffer, packet_p->length, (packet_p->length -1));
  packet_p->length--;

  return PARSE_OK;
}
