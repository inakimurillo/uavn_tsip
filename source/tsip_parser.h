#ifndef H_TSIP_PARSER_INCLUDED
#define H_TSIP_PARSER_INCLUDED
#include "common_types.h"

/**
 * \brief Extra DLE bytes extraction
 *
 * length parameter will be updated
 *
 * \param[in] data Pointer to the array
 * \param[in] length Number of bytes in the data array
 * \return 0 if extraction is successful, 1 otherwise
 */
uint32_t dle_etx_extraction (uint8_t *data, uint32_t *length);

/**
 * \brief Integrity check of the array
 *
 * \param[in] data Pointer to the array
 * \param[in] length Number of bytes in the data array
 * \return 0 if extraction is successful, 1 otherwise
 */
uint32_t integrity_check (uint8_t *data, uint32_t length);

/**
 * \brief Unpacket data from TSIP packet
 *
 * \param[in] packet_p Pointer to the TSIP packet
 * \return parser_err enumeration
 */
parser_err tsip_packet_parser(tsip_packet *packet_p);

#endif
