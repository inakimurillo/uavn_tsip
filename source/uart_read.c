#include "uart_read.h"
#include "uav_functions.h"

#define TSIP_PACKET_MAX_LEN 256

simple_buffer dma_buffer = {.read_p = 0, .write_p = 0};

uint8_t tspi_buff[TSIP_PACKET_MAX_LEN];
uint32_t tspi_buff_length;

uart_status perform_uart_read(tsip_packet *packet)
{
  uint32_t read_bytes;
  uint32_t posible_end = 0;


  /// read from uart in case there is no more data inside local buffer1
  if ((dma_buffer.write_p == 0) || (dma_buffer.write_p == dma_buffer.read_p))
  {
    read_bytes = uavnComRead(dma_buffer.data, DMA_BUFFER_SIZE);
    if(read_bytes == 0)
    {
      return NO_DATA;
    }
    /// reset pointers
    dma_buffer.write_p = read_bytes;
    dma_buffer.read_p = 0;
  }

  /// go over the local buffer
  for(; dma_buffer.read_p < dma_buffer.write_p; dma_buffer.read_p++)
  {
    if((dma_buffer.data[dma_buffer.read_p] == DLE_BYTE) && (posible_end == 0))
    {
      /// if the next byte is ETX_BYTE, then is the end of the packet
      posible_end = 1;
    }
    else if((dma_buffer.data[dma_buffer.read_p]  == ETX_BYTE) && (posible_end == 1))
    {
      // copy last byte
      tspi_buff[tspi_buff_length] = dma_buffer.data[dma_buffer.read_p];
      tspi_buff_length++;
      // advance local buffer read pointer
      dma_buffer.read_p++;

      // Fill TSIP packet
      packet->buffer = (uint8_t *)tspi_buff;
      packet->length = tspi_buff_length;

      /// reset local buffer's length
      tspi_buff_length = 0;

      // Return with TSIP packet
      return DATA_AVAILABLE;
    }
    else
    {
      posible_end = 0;
    }

    /// copy data into buffer for TSIP parser
    tspi_buff[tspi_buff_length] = dma_buffer.data[dma_buffer.read_p];
    tspi_buff_length++;
  }

  return NO_DATA;
}
