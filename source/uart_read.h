#ifndef H_UART_READ_INCLUDED
#define H_UART_READ_INCLUDED

#include "common_types.h"

#define DMA_BUFFER_SIZE 512

typedef enum uart_status{
  DATA_AVAILABLE,
  NO_DATA,
  UART_ERR
}uart_status;

typedef struct {
  uint32_t read_p;
  uint32_t write_p;
  uint8_t data[DMA_BUFFER_SIZE];
}simple_buffer;

/**
 * \brief Read from uart
 *
 * \param[out] packet Pointer to the TSIP packet
 * \return uart_status enumeration
 */
uart_status perform_uart_read(tsip_packet *packet);

#endif
