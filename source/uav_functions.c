#include "uav_functions.h"
#include "common_types.h"
#include "tsip_parser.h"
#include "uart_read.h"
#include <stdio.h>

/// Used for uavnComRead
uint32_t done = 0;

void ParseTsipData(const uint8_t * const buffer, const int32_t numberOfBytes)
{
  uint8_t original_data[6] =  {0x02,0x01,0x10,0xA4,0x10,0xE1 };

  uint32_t index;

  if(numberOfBytes != 6)
  {
    printf(" ERROR!\n" );
    return;
  }

  for (index = 0; index < numberOfBytes; index++)
  {
    if(buffer[index] != original_data[index])
    {
      printf(" ERROR!\n" );
    }
    printf("0x%02X,", buffer[index]);
  }
  printf("\n");
}


int32_t uavnComRead(uint8_t * const buffer, const uint32_t count)
{
  uint8_t data1[80] =      {0x10,0x04,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0x03,      0x10,0x04,0x03,0x02,0x01,0x10,0x10,0xA4,0x10};
  uint8_t data2[80] =      {0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0x03,      0x10,0x04,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0x03};
  uint8_t data3[80] =      {0x10,0x04,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0x03,      0x10,0x04,0x03,0x02,0x01,0x10,0x10,0xA4,0x10,0x10,0xE1,0x29,0x21,0x8F,0xB3,0x10,0x03};
  uint32_t index;


  switch (done) {
    case 1:
    for(index = 0; index < 17*2; index++)
    {
      buffer[index] = data1[index];
    }
    done++;
    printf("--- CASE 1 ---\n");
    return 26;
    break;
    case 2:
    for(index = 0; index < 17*2; index++)
    {
      buffer[index] = data2[index];
    }
    done++;
    printf("--- CASE 2 ---\n");
    return 25;
    break;

    case 3:
    for(index = 0; index < 17*2; index++)
    {
      buffer[index] = data3[index];
    }
    done++;
    printf("--- CASE 3 ---\n");
    return 17*2;
    break;
  }

  done++;

  return 0;
}


void TaskUplink200Hz(void)
{
  tsip_packet packet_p;
  uart_status stat;
  parser_err err;

  stat = perform_uart_read(&packet_p);

  if(stat == DATA_AVAILABLE)
  {
    err = tsip_packet_parser(&packet_p);
    switch (err) {
      case PARSE_OK:
        ParseTsipData(packet_p.buffer, packet_p.length);
        break;
      case INTEGRITY_ERR:
        /// Handle error
        break;
      case DLE_EXTRACTION_ERR:
        /// Handle error
        break;
      case CRC_ERR:
        /// Handle error
        break;
      default:
        /// Handle error
        break;
    }
  }
}
